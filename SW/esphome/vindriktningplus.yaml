substitutions:
  # pinout
  FAN_PIN: GPIO4
  BUZZER_PIN: GPIO5
  WS2812_PIN: GPIO2
  BRIGHT_PIN: GPIO3
  PM_TX_PIN: GPIO0
  PM_RX_PIN: GPIO1
  IR_REC_PIN: GPIO10
  I2C_SCL: GPIO7
  I2C_SDA: GPIO8
  # BOOT_PIN: GPIO9
  INT_LED_PIN: GPIO6
  # default values
  default_led_brightness: "50"
  scd4x_update_interval: 30s
  pm1006_update_interval: 30s
  bme280_update_interval: 30s
  adc_update_interval: 30s
  led_change_interval: 15s
  pos_latitude: "48.210033"
  pos_longitude: "16.363449"
  pos_altitude: "158.3"
  baro_top: "1050.0"
  baro_bottom: "950.0"
  # template stuff
  name: "vindriktning"
  friendly_name: "Vindriktning+ Template"
  timezone: "Europe/Vienna"
  scd4x_temp_offset: "6"
  bme280_temp_offset: "6"

# Enable logging
logger:

# Enable bluetooth proxy
# bluetooth_proxy:
#   active: true

# Enable Home Assistant API
api:
  encryption: 
    key: "replace me!"  #get your own keye from here: https://esphome.io/components/api.html#configuration-variables

ota:
  - platform: esphome
    password: "replace me!" #get your own password from f.e. here: https://www.nayuki.io/page/random-password-generator-javascript

wifi:
  networks: 
    ssid: !secrets wifi_ssid
    password: !secrets wifi_password


# This should point to the public location of this yaml file.
dashboard_import:
  package_import_url: gitlab://home-automation9622510/VindriktningPlus/SW/esphome/vindriktningplus.yaml@main
  import_full_config: true

globals:
  - id: glob_brightness
    type: int
    restore_value: True
    initial_value: $default_led_brightness

esphome:
  platformio_options:
    board_build.flash_mode: dio
    build_flags:
      - -std=c++17
    build_unflags: 
      - -std=c++11
      - -std=gnu++11
  name: $name
  friendly_name: $friendly_name

  on_boot:
    - priority: 240
      then:
        - uart.write:
            id: uart_a
            data: [0x11, 0x02, 0x0B, 0x01, 0xE1]
    - priority: -10
      then:
        - lambda: "id(pm2_5).publish_state(1.0);"
        - light.turn_on:
            id: rgb_led
            brightness: !lambda return id(led_brightness).state;
            effect: "Boot Pulse"
        - delay: 5s
        - wait_until:
            timeout: 15s
            condition:
              and:
                - wifi.connected:
                - api.connected:
        - light.turn_off: rgb_led
        - if:
            condition:
              - switch.is_on: led_auto
            then:
              - script.execute: change_led_mode

esp32:
  board: esp32-c3-devkitm-1
  framework:
    type: esp-idf

number:
  # LED brightness in day mode (in percent)
  - platform: template
    id: led_brightness
    name: "Led Brightness"
    min_value: 0
    max_value: 100
    step: 1
    optimistic: True
    restore_value: True
    entity_category: "config"
    unit_of_measurement: "%"
    on_value:
      then:
        - script.execute: update_led_brightness

  # LED brightness in night mode (in percent)
  - platform: template
    id: led_night_mode
    name: "Led Night Brightness"
    min_value: 0
    max_value: 100
    step: 1
    optimistic: True
    restore_value: True
    entity_category: "config"
    unit_of_measurement: "%"
    on_value:
      then:
        - script.execute: update_led_brightness

  # Automatic fan purge duration
  - platform: template
    id: fan_durge_duration
    name: "Fan purge duration"
    unit_of_measurement: s
    optimistic: True
    restore_value: True
    min_value: 0
    max_value: 30
    step: 1
    entity_category: "config"

binary_sensor:
  - platform: status
    id: api_mode
    internal: True

button:
  # Button to force an measuremnt cycle for the PM1006
  - platform: template
    name: "Force PM1006 measurement"
    on_press:
      then:
        - script.execute: pm1006_measurement_script

switch:
  # pseudo switch to switch between day and night mode
  #   night mode deactivats the PM1006 measurements, since the fan has to run
  - platform: template
    name: "Night Mode"
    id: night_mode
    turn_on_action:
      - lambda: id(night_mode).publish_state(true);
      - script.execute: update_led_brightness
    turn_off_action:
      - lambda: id(night_mode).publish_state(false);
      - script.execute: update_led_brightness

  # pseudo switch to enable internal LED control
  - platform: template
    name: "Automatic LED mode"
    entity_category: "config"
    id: led_auto
    optimistic: True
    restore_mode: RESTORE_DEFAULT_ON

  # The Fan pin, this is either controlled by script or by the user
  - platform: gpio
    pin: $FAN_PIN
    id: pm1006_fan
    name: "PM1006 Fan"
    restore_mode: ALWAYS_OFF

# uart used for PM1006 exclusively
uart:
  tx_pin: $PM_TX_PIN
  rx_pin: $PM_RX_PIN
  baud_rate: 9600
  id: uart_a

# main i2c bus
i2c:
  scl: $I2C_SCL
  sda: $I2C_SDA
  id: bus_a
  frequency: 400kHz
  scan: true

text_sensor:
  # ESPHome version string
  - platform: version
    name: ESPHome Version
  # fancy wifi information
  - platform: wifi_info
    ip_address:
      name:  IP
    ssid:
      name: SSID
    bssid:
      name: BSSID
  # Sun information
  - platform: sun
    name: Sun Next Sunrise
    type: sunrise
  - platform: sun
    name: Sun Next Sunset
    type: sunset

select:
  - platform: template
    name: "Fan mode select"
    id: fan_mode
    optimistic: True
    icon: mdi:fan-auto
    entity_category: "config"
    restore_value: True
    options: 
      - Automatic
      - Automatic ignore night mode
      - Manual

sensor:
  # board uptime
  - platform: uptime
    name: Uptime
  # WiFi signal information
  - platform: wifi_signal
    name: WiFi Signal
    update_interval: 60s

  # The PM1006 particle sensor in the Vindrikting
  - platform: pm1006
    uart_id: uart_a
    id: pm1006_module
    update_interval: never
    pm_2_5:
      internal: True
      id: _pm1006_pm_2_5

  # PM2.5 sensor which'll get updated by interval
  - platform: template
    name: "Particulate Matter 2.5µm Concentration"
    id: "pm2_5"
    unit_of_measurement: "µg/m³"
    icon: "mdi:blur"

  # CO2, temperature & humidity sensor
  - platform: scd4x
    i2c_id: bus_a
    co2:
      name: "SCD40 CO2"
      id: co2_sdx
    temperature:
      name: "SCD40 Temperature"
      id: temperature_sdx
    humidity:
      name: "SCD40 Humidity"
      id: humidity_sdx
    measurement_mode: low_power_periodic
    update_interval: $scd4x_update_interval
    temperature_offset: $scd4x_temp_offset
    ambient_pressure_compensation_source: bme_pressure

  # BME280 temperature, humididty & air pressure sensor
  - platform: bme280_i2c
    i2c_id: bus_a
    address: 0x76
    temperature:
      name: "BME280 Temperature"
      id: bme_temperature
      oversampling: 8x
      filters:
        - offset: -$bme280_temp_offset
    humidity:
      name: "BME280 Humidity"
      id: bme_humidity
    pressure:
      name: "BME280 Pressure"
      id: bme_pressure
    update_interval: $bme280_update_interval

  # dew point
  - platform: template
    name: "Dew Point"
    lambda: |-
      return (243.5*(log(id(bme_humidity).state/100)+((17.67*id(bme_temperature).state)/
      (243.5+id(bme_temperature).state)))/(17.67-log(id(bme_humidity).state/100)-
      ((17.67*id(bme_temperature).state)/(243.5+id(bme_temperature).state))));
    unit_of_measurement: °C
    icon: 'mdi:thermometer-alert'

  # equivalent sea level pressure
  - platform: template
    name: "Equivalent sea level pressure"
    id: sea_level_pressure
    lambda: |-
      // push current value to "old container"
      constexpr float STANDARD_ALTITUDE = $pos_altitude; // in meters, see note
      return id(bme_pressure).state / powf(1 - ((0.0065 * STANDARD_ALTITUDE) / (id(temperature_sdx).state + (0.0065 * STANDARD_ALTITUDE) + 273.15)), 5.257); // in hPa
    unit_of_measurement: 'hPa'

  # calculate the absolute humidity
  - platform: absolute_humidity
    name: "Absolute Humidity"
    temperature: bme_temperature
    humidity: bme_humidity

  # calculated Heat Index (HI)
  - platform: template
    name: "Heat Index"
    id: heat_index
    unit_of_measurement: '°C'
    update_interval: $scd4x_update_interval
    lambda: |-
      // calculate HI according to wikipedia
      constexpr double c1 = -8.78469475556;
      constexpr double c2 = 1.61139411;
      constexpr double c3 = 2.33854883889;
      constexpr double c4 = -0.14611605;
      constexpr double c5 = -0.012308094;
      constexpr double c6 = -0.0164248277778;
      constexpr double c7 = 2.211732E-3;
      constexpr double c8 = 7.2546E-4;
      constexpr double c9 = -3.582E-6;
      const auto T = id(temperature_sdx).state;
      const auto R = id(humidity_sdx).state;

      const double hi = c1 + c2*T + c3*R + c4*R*T + c5*std::pow(T,2) + c6*std::pow(R,2) + c7*std::pow(T,2)*R + c8*T*std::pow(R,2) + c9*std::pow(T*R, 2);
      return hi;

  # Phototransistor
  - platform: adc
    pin: $BRIGHT_PIN
    name: Illuminance
    unit_of_measurement: lux
    attenuation: 12db
    update_interval: $adc_update_interval
    filters:
      - sliding_window_moving_average:
          window_size: 15
          send_every: 15
      - lambda: |-
          return (x * 3.3 / 10000.0) * 2000000.0;    

output:
  - platform: ledc
    pin: $BUZZER_PIN
    channel: 0
    id: buzzer_out

# remote_receiver:
#   pin: $IR_REC_PIN
#   dump: all

rtttl:
  output: buzzer_out
  id: rtttl_module

light:
  - platform: esp32_rmt_led_strip
    name: "LEDs"
    pin: GPIO2
    rgb_order: GRB
    num_leds: 3
    rmt_channel: 0
    chipset: WS2812
    id: rgb_led
    color_correct: [100%, 100%, 80%]
    default_transition_length: 0s
    on_turn_on:
      - then:
        - light.turn_on: 
            id: rgb_led
            brightness: !lambda "return float(id(glob_brightness))/100.0f;"
    effects:
      # pulsing light for boot-up until the unit is connected to wifi & ha
      - pulse:
          transition_length: 0.5s
          update_interval: 0.5s
          min_brightness: 10%
          max_brightness: 60%
          name: "Boot Pulse"
      - addressable_lambda:
          name: "Quality sensors"
          update_interval: 1s
          lambda: |-
            static constexpr size_t pm2Led = 0;
            static constexpr size_t co2Led = 1;
            static constexpr size_t hiLed = 2;

            it.all() = Color::BLACK;

            {
              // WHO2015: AQI Concentration Breakpoints by Pollutant 
              const std::array<std::tuple<uint16_t,uint16_t, Color>, 6> ranges = {{
                {  0, 12,  Color(0x00, 0xFF, 0x20)}, // green
                { 12, 35,  Color(0xff, 0xed, 0x2B)}, // yellow
                { 35, 55,  Color(0xff, 0x99, 0x33)}, // orange
                { 55, 150, Color(0xcc, 0x00, 0x33)}, // red
                {150, std::numeric_limits<uint16_t>::max(), Color(0xAD, 0x1F, 0x3C)}, // violette
              }};  
              // execute the led coloring
              const auto sensor_value = id(pm2_5).state;
              for(const auto &e: ranges) {
                const auto [lower, upper, color] = e;
                if(sensor_value >= lower && sensor_value < upper) {
                  it[pm2Led] = color;
                  break;
                }
              }
            }

            {
              const std::array<std::tuple<uint16_t, uint16_t, Color>, 5> ranges = {{
                  {0,     700, Color(0x00, 0xFF, 0x20)},  // green
                  {700,  1000, Color(0xff, 0xed, 0x2B)},  // yellow
                  {1000, 1500, Color(0xff, 0x99, 0x33)},  // orange
                  {1500, 2100, Color(0xcc, 0x00, 0x33)},  // red
                  {2100,  std::numeric_limits<uint16_t>::max(), Color(0xAD, 0x1F, 0x3C)}   // violette
              }};
              // execute the led coloring
              const auto sensor_value = id(co2_sdx).state;
              for(const auto &e: ranges) {
                const auto [lower, upper, color] = e;
                if(sensor_value >= lower && sensor_value < upper) {
                  it[co2Led] = color;
                  break;
                }
              }
            }

            {
              const std::array<std::tuple<uint16_t, uint16_t, Color>, 5> ranges = {{
                  {0,  27, Color(0x00, 0xFF, 0x20)},  // green
                  {27, 32, Color(0xff, 0xed, 0x2B)},  // yellow
                  {32, 40, Color(0xff, 0x99, 0x33)},  // orange
                  {40, 54, Color(0xcc, 0x00, 0x33)},  // red
                  {54, std::numeric_limits<uint16_t>::max(), Color(0xAD, 0x1F, 0x3C)}   // violette
              }};
              // execute the led coloring
              const auto sensor_value = id(heat_index).state;
              for(const auto &e: ranges) {
                const auto [lower, upper, color] = e;
                if(sensor_value >= lower && sensor_value < upper) {
                  it[hiLed] = color;
                  break;
                }
              }
            }

            it.schedule_show();

status_led:
  pin: $INT_LED_PIN

# optional lcd
# font:
#   - file: "gfonts://Roboto"
#     id: roboto
#     size: 10

# display:
#   - platform: ssd1306_i2c
#     model: SSD1306_128X32
#     id: mini_display
#     pages:
#       - id: page1
#         lambda: |-
#           it.printf(5, 10, id(roboto), "PM2.5 %0.2f µg/m³", id(pm_2_5).state);
#           it.printf(5, 15, id(roboto), "Temperature: %0.2f °C", id(scd4x_temperature).state);
#           it.printf(5, 20, id(roboto), "Humidity: %0.2f %", id(scd4x_humidity).state);
#       - id: page2
#         lambda: |-
#           it.strftime(10, 15, id(roboto), "%Y-%m-%d %H:%M", id(ha_time).now());

script:
  - id: pm1006_measurement_script
    mode: single
    then:
      - if:
          condition:
            - lambda: |-
                // get if we should use a fan
                const auto modeIdx = id(fan_mode).active_index().value_or(0);
                switch(modeIdx) {
                  case 0: // automatic with nightmode
                    return !id(night_mode).state;

                  case 1: // automatic ignoring nightmode
                    return true;

                  default:
                    return false;
                }
          then:
            - switch.turn_on: pm1006_fan
            - delay: !lambda "return id(fan_durge_duration).state * 1000;"
            - switch.turn_off: pm1006_fan
      - lambda: |-
          id(pm1006_module).update();
          id(pm2_5).publish_state(id(_pm1006_pm_2_5).state);
  - id: update_led_brightness
    mode: single
    then:
      - lambda: |-
          const auto lambda = [](light::AddressableLightState *light, float brightness){
            if(light->current_values.is_on()) {
              auto call = light->turn_on();
              call.set_brightness(float(brightness)/100.0f);
              call.perform();
            }
          };
          const auto val = id(night_mode).state ? id(led_night_mode).state : id(led_brightness).state;
          id(glob_brightness) = val;
          lambda(id(rgb_led), val);
  - id: change_led_mode
    mode: single
    then:
      - lambda: |-
          static uint8_t animation = 0;
          static constexpr std::array effects = {
            "Quality sensors",
          };

          auto call = id(rgb_led).turn_on();
          call.set_effect(effects[animation]);
          call.perform();

          animation++;
          if(animation >= effects.size())
            animation = 0;

sun:
  latitude: ${pos_latitude}
  longitude: ${pos_longitude}
  on_sunrise:
    then:
      - logger.log: Good morning!
      - if:
          condition:
            lambda: 'return !(id(api_mode).state && !id(led_auto).state);'
          then:
            - switch.turn_off: night_mode
  on_sunset:
    then:
      - logger.log: Good evening!
      - if:
          condition:
            lambda: 'return !(id(api_mode).state && !id(led_auto).state);'
          then:
            - switch.turn_on: night_mode

interval:
  - interval: $pm1006_update_interval
    id: pm1006_measurement
    then:
      - if:
          condition:
            - switch.is_off: night_mode
          then:
            - script.execute: pm1006_measurement_script
  - interval: $led_change_interval
    id: led_standalone
    then:
      - if:
          condition:
            - switch.is_on: led_auto
          then:
            - script.execute: change_led_mode


time:
  - platform: homeassistant
    id: ha_time
    timezone: $timezone
  - platform: sntp
    id: sntp_time
    timezone: $timezone
